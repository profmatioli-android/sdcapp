#Branch Versão 1
Cria a estrutura para exibir uma lista personalizada.

#Branch Versão 2
Acrescentado Requisição HTTP para a API do SoDeCenoura em:
http://143.106.241.1/matioli/sodecenoura/api/categoria

#Branch Versão 3
Acrescentado Banco de Dados local, SQLite.
Acrescentado Botão para sincronização com o servidor remoto

#SOBRE OS RECURSOS UTILIZADO NESTE EXEMPLO

##Para consulta sobre como criar uma ListView personalizada, sugiro:
http://blog.alura.com.br/personalizando-uma-listview-no-android/


##Para consulta sobre requisições HTTP com a API OkHttp, utilizada neste projeto, sugiro:
http://square.github.io/okhttp/
https://www.journaldev.com/13629/okhttp-android-example-tutorial

##Para consulta sobre SQLite sugiro:
https://developer.android.com/training/data-storage/sqlite#java
https://www.androidpro.com.br/blog/armazenamento-de-dados/sqlite/